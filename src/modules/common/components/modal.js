import React, { useState } from "react";
import { Modal, Tabs, Tab } from "react-bootstrap";
import { Link } from "react-router-dom";
import Terms from "./terms";
import styled from "styled-components";

const ButtonAccept = styled.button`
  width: 150px;
  padding: 11px 7px;
  border-radius: 22px 22px;
  margin: 10px;
  color: white;
  background-color: #0671b2;
  border: none;
  font-size: 15px;
  font-weight: 550;
  :disabled {
    opacity: 0.4;
  }
  :focus {
    outline: none;
  }
`;
const ButtonCancel = styled.button`
  width: 150px;
  padding: 9px 7px;
  border-radius: 22px 22px;
  margin: 10px;
  color: #0671b2;
  background-color: white;
  font-size: 15px;
  font-weight: 550;
  border: 2px solid #0671b2;
  :focus {
    outline: none;
  }
`;
const StatementDiv = styled.div`
  background-color: #f1f1f1;
  border-radius: 6px 6px;
  padding: 13px;
  margin: 3px 12px 19px 3px;
`;

// for inlining styling of modal components

const modal = (props) => {
  const [isActive, setIsActive] = useState(false);

  const checkbox = {
    marginRight: "10px",
  };
  const modalBody = {
    height: "400px",
  };
  const customModal = {
    overflowY: "scroll",
    maxHeight: "400px",
    height: "100%",
  };
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className="modalMain"
    >
      <Modal.Header>
        <Modal.Title id="contained-modal-title-vcenter">
          Terms and Privacy Policy
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={modalBody}>
        <div style={customModal}>
          <StatementDiv>
            <h4>Statement of intent</h4>
            <p style={{ fontSize: "13px", fontWeight: "200" }}>
              Cras mattis consectetur purus sit amet fermentum. Cras justo odio,
              dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta
              ac consectetur ac, vestibulum at eros.
            </p>
          </StatementDiv>
          <div>
            <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
              <Tab eventKey="home" title="Terms of Use">
                <Terms />
              </Tab>
              <Tab eventKey="profile" title="Policy">
                <Terms />
              </Tab>
            </Tabs>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <form>
          <input
            onChange={() => setIsActive((prevState) => !prevState)}
            style={checkbox}
            type="checkbox"
            name="privacy policy"
          />
          <label>I have read and agree to the terms and privacy policy</label>
        </form>
        <div>
          <ButtonCancel onClick={props.onHide}>Cancel</ButtonCancel>

          <Link exact={true} to="register">
            <ButtonAccept disabled={!isActive} onClick={props.accepted}>
              Accept
            </ButtonAccept>
          </Link>
        </div>
      </Modal.Footer>
    </Modal>
  );
};

export default modal;
